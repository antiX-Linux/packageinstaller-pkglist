<?xml version="1.0"?>
<app>

<category>
Graphics
</category>

<name>  
Blender
</name>

<description>  
3D animation suite
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
blender
libgl1-mesa-dri
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
blender
libgl1-mesa-dri
</uninstall_package_names>
</app>