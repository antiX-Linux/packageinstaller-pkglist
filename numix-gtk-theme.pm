<?xml version="1.0"?>
<app>

<category>
Themes
</category>

<name>  
Numix Gtk Theme
</name>

<description>  
A modern flat theme with a combination of light and dark elements
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
numix-gtk-theme
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
numix-gtk-theme
</uninstall_package_names>
</app>
