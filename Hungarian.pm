<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
Hungarian
</name>

<description>  
Hungarian dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-hu
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-hu
</uninstall_package_names>

</app>
