<?xml version="1.0"?>
<app>

<category>
Email
</category>

<name>  
Claws-Mail
</name>

<description>  
Claws Mail lightweight email client
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
claws-mail
claws-mail-i18n
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
claws-mail
claws-mail-i18n
</uninstall_package_names>
</app>