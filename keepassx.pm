<?xml version="1.0"?>
<app>

<category>
Misc
</category>

<name>  
KeepassX
</name>

<description>  
Password manager
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
keepassx
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
keepassx
</uninstall_package_names>
</app>