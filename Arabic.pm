<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>  
Arabic
</name>

<description>  
Arabic dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-ar
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-ar
</uninstall_package_names>

</app>
