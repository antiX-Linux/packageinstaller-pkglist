<?xml version="1.0"?>
<app>

<category>
File Managers
</category>

<name>  
PCManFM
</name>

<description>  
Fast and lightweight file manager
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
pcmanfm
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
pcmanfm
</uninstall_package_names>
</app>