<?xml version="1.0"?>
<app>

<category>
Desktop Environment
</category>

<name>  
KDE5
</name>

<description>  
KDE5-full
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
kde-full
kwin-x11
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
kde-full
kwin-x11
</uninstall_package_names>

</app>
