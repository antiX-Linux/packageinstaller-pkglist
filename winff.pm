<?xml version="1.0"?>
<app>

<category>
Media Converter
</category>

<name>  
WinFF 
</name>

<description>  
Graphical video and audio batch converter 
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
winff
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
winff
</uninstall_package_names>
</app>