<?xml version="1.0"?>
<app>

<category>
Browser
</category>

<name>
Waterfox Classic
</name>

<description>
Alternative mozilla-based browser without extensions
</description>

<installable>
64
</installable>

<screenshot>https://www.waterfoxproject.org/media/img/waterfox/products/desktop/waterfox-browser.40990c516643.svg</screenshot>

<preinstall>
</preinstall>

<install_package_names>
waterfox-classic-kpe
</install_package_names>


<postinstall>
if [ "$(locale |grep LANG|cut -d= -f2 |cut -d_ -f1)" != "en" ]; then
apt-get install waterfox-classic-i18n-$(locale |grep LANG|cut -d= -f2 |cut -d_ -f1)
fi
</postinstall>


<uninstall_package_names>
waterfox-classic-kpe
</uninstall_package_names>
</app>
