<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>  
Dutch_LO_main
</name>

<description>  
Dutch LibreOffice Language Meta-Package (no database)
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-nl
libreoffice-help-nl
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-nl
libreoffice-help-nl
libreoffice-gtk3
</uninstall_package_names>

</app>
