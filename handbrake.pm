<?xml version="1.0"?>
<app>

<category>
Media Converter
</category>

<name>  
Handbrake
</name>

<description>  
Versatile DVD ripper and video transcoder (cli and GUI)
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
handbrake
handbrake-cli
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
handbrake
handbrake-cli
</uninstall_package_names>
</app>