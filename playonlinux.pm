<?xml version="1.0"?>
<app>

<category>
Games
</category>

<name>
Play on Linux
</name>

<description>
Play on Linux
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
wine-staging
winehq-staging
wine-staging-compat
playonlinux

</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
playonlinux
</uninstall_package_names>
</app>
