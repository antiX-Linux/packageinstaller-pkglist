<?xml version="1.0"?>
<app>

<category>
Audio
</category>

<name>  
Pulseaudio
</name>

<description>  
Advanced sound server system
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
pulseaudio 
rtkit 
pavucontrol
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
pulseaudio
rtkit
pavucontrol
</uninstall_package_names>
</app>