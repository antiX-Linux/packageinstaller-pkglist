<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
Norwegian
</name>

<description>  
Norwegian dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-no
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-no
</uninstall_package_names>

</app>
