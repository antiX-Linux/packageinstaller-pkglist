<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
Sinhala
</name>

<description>  
Sinhala dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-si
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-si
</uninstall_package_names>

</app>
