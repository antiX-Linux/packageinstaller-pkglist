<?xml version="1.0"?>
<app>

<category>
Desktop Environment
</category>

<name>  
Cinnamon-Standard
</name>

<description>  
Cinnamon desktop
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
cinnamon
elogind
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
cinnamon
</uninstall_package_names>
</app>