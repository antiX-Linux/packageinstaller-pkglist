<?xml version="1.0"?>
<app>

<category>
Network
</category>

<name>  
Droopy
</name>

<description>  
Simple way to share files via web server
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
droopy
</install_package_names>

<uninstall_package_names>
droopy
</uninstall_package_names>
</app>
