<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>  
Japanese_Input
</name>

<description>  
Japanese ibus
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
im-config
ibus-anthy
ibus-mozc
ibus-gtk
fonts-vlgothic
manpages-ja
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
im-config
ibus-anthy
ibus-mozc
ibus-gtk
fonts-vlgothic
manpages-ja
</uninstall_package_names>

</app>
