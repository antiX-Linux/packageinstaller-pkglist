<?xml version="1.0"?>
<app>

<category>
Torrent
</category>

<name>  
qBittorrent
</name>

<description>  
QT feature rich but lightweight client similar to uTorrent
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
qbittorrent
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
qbittorrent
</uninstall_package_names>
</app>