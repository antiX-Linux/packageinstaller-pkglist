<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
Austrian (German)
</name>

<description>  
Austrian (German) dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-de-at
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-de-at
</uninstall_package_names>

</app>
