<?xml version="1.0"?>
<app>

<category>
Network
</category>

<name>  
Connman
</name>

<description>  
Connection Manager is designed to be a light network manager
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
connman

</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
connman
</uninstall_package_names>
</app>