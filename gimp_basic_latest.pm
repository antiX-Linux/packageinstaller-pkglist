<?xml version="1.0"?>
<app>

<category>
Graphics
</category>

<name>  
Latest GIMP Basic
</name>

<description>  
Advanced picture editor - installs GIMP only
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 276ECD5CEF864D8F
echo "deb http://mxrepo.com/mx/repo/ buster main non-free">/etc/apt/sources.list.d/pitemp1.list
sleep .1
echo "deb http://mxrepo.com/mx/testrepo/ buster test ">/etc/apt/sources.list.d/pitemp.list
apt-get update
</preinstall>

<install_package_names>
-t mx gimp
</install_package_names>


<postinstall>
rm /etc/apt/sources.list.d/pitemp.list
rm /etc/apt/sources.list.d/pitemp1.list
apt-get update
</postinstall>


<uninstall_package_names>
gimp
</uninstall_package_names>
</app>