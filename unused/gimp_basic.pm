<?xml version="1.0"?>
<app>

<category>
Graphics
</category>

<name>  
GIMP Basic
</name>

<description>  
advanced picture editor - installs GIMP only
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
gimp
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
gimp
</uninstall_package_names>
</app>