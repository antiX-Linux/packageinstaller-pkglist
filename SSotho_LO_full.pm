<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>  
SSotho_LO_full
</name>

<description>  
Southern_sotho Language Meta-Package for LibreOffice.
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-st
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-st
libreoffice-gtk3
</uninstall_package_names>

</app>
