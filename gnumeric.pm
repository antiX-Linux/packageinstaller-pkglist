<?xml version="1.0"?>
<app>

<category>
Office
</category>

<name>  
Gnumeric
</name>

<description>  
Lightweight spreadsheet
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
gnumeric
gnumeric-common
gnumeric-doc
gnumeric-plugins-extra
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
gnumeric
gnumeric-common
gnumeric-doc
gnumeric-plugins-extra
</uninstall_package_names>
</app>