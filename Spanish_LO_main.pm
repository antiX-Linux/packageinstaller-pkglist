<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>  
Spanish_LO_main
</name>

<description>  
Spanish Language Meta-Package for LibreOffice (no database)
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-es
libreoffice-help-es
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-es
libreoffice-help-es
libreoffice-gtk3
</uninstall_package_names>

</app>
