<?xml version="1.0"?>
<app>

<category>
Children
</category>

<name>  
Secondary
</name>

<description>  
Secondary. Includes: calibre, laby, lightspeed, melting, ri-li and stellarium
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
calibre
laby
lightspeed
melting
ri-li
stellarium
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
calibre
laby
lightspeed
melting
ri-li
stellarium
</uninstall_package_names>
</app>