<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
Hebrew
</name>

<description>  
Hebrew dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-he
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-he
</uninstall_package_names>

</app>
