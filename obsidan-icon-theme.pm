<?xml version="1.0"?>
<app>

<category>
Icons
</category>

<name>  
Obsidian Icon Theme
</name>

<description>  
Fork of Faenza, primary developed for dark themes. 
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
obsidian-icon-theme
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
obsidian-icon-theme
</uninstall_package_names>
</app>
