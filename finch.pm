<?xml version="1.0"?>
<app>

<category>
Messaging
</category>

<name>  
Finch
</name>

<description>  
Text-console-based, modular instant messaging client
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
finch
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
finch
</uninstall_package_names>
</app>