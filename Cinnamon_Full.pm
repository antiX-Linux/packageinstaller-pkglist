<?xml version="1.0"?>
<app>

<category>
Desktop Environment
</category>

<name>  
Cinnamon-Full
</name>

<description>  
Cinnamon desktop
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
cinnamon-desktop-environment
elogind
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
cinnamon-desktop-environment
</uninstall_package_names>
</app>