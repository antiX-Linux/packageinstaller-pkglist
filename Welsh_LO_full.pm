<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>  
Welsh_LO_full
</name>

<description>  
Welsh LibreOffice Language Meta-Package
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-cy
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-gtk3
libreoffice-l10n-cy
</uninstall_package_names>

</app>
