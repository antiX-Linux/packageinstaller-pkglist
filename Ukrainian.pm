<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
Ukrainian
</name>

<description>  
Ukrainian dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-uk
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-uk
</uninstall_package_names>

</app>
