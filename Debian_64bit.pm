<?xml version="1.0"?>
<app>

<category>
Kernel
</category>

<name>  
Kernel-Debian_64bit_meltdown-patched
</name>

<description>  
Fallback Debian 4.19 64bit linux kernel (Meltdown patched)
</description>

<installable>
64
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
linux-image-4.19.0-13-amd64
linux-headers-4.19.0-13-amd64
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
linux-image-4.19.0-13-amd64
linux-headers-4.19.0-13-amd64
</uninstall_package_names>

</app>
