<?xml version="1.0"?>
<app>

<category>
Themes
</category>

<name>  
Obsidian-2 Gtk Theme
</name>

<description>  
Dark Gtk Theme
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
obsidian-2-gtk-theme
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
obsidian-2-gtk-theme
</uninstall_package_names>
</app>
