<?xml version="1.0"?>
<app>

<category>
Package Management
</category>

<name>
synaptic
</name>

<description>
Graphical package management program for apt
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
synaptic
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
synaptic
</uninstall_package_names>
</app>
