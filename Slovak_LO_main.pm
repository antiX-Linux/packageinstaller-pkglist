<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>  
Slovak_LO_main
</name>

<description>  
Slovenian Language Meta-Package for LibreOffice (no database)
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-sk
libreoffice-help-sk
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-sk
libreoffice-help-sk
libreoffice-gtk3
</uninstall_package_names>

</app>
