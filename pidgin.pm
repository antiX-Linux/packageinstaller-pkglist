<?xml version="1.0"?>
<app>

<category>
Messaging
</category>

<name>  
Pidgin
</name>

<description>  
Easy to use chat client
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
pidgin
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
pidgin
</uninstall_package_names>
</app>