<?xml version="1.0"?>
<app>

<category>
Office
</category>

<name>  
Abiword
</name>

<description>  
Lightweight word processor
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
abiword
abiword-common
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
abiword
abiword-common
</uninstall_package_names>
</app>