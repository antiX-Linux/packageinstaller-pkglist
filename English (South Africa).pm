<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
English (South Africa)
</name>

<description>  
English (South Africa) dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-en-za
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-en-za
</uninstall_package_names>

</app>
