<?xml version="1.0"?>
<app>

<category>
Development
</category>

<name>  
QT5 Dev Enviroment
</name>

<description>  
QT5 Development Environment
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
qtcreator
qt5-default
qttools5-dev-tools
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
qtcreator
qt5-default
qttools5-dev-tools
</uninstall_package_names>
</app>
