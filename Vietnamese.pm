<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
Vietnamese
</name>

<description>  
Vietnamese dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-vi
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-vi
</uninstall_package_names>

</app>
