<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
Malayalam
</name>

<description>  
Malayalam dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-ml
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-ml
</uninstall_package_names>

</app>
