<?xml version="1.0"?>
<app>

<category>
Server
</category>

<name>  
Local Web Server
</name>

<description>  
apache2, php7, mariadb server applications
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
apache2
apache2-utils
php7.3
php7.3-mysql
php7.3-common 
mariadb-client
mariadb-server 
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
apache2
apache2-utils
php7.3
php7.3-mysql
php7.3-common 
mariadb-client
mariadb-server 
</uninstall_package_names>
</app>