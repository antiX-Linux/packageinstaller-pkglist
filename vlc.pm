<?xml version="1.0"?>
<app>

<category>
Video
</category>

<name>  
VLC
</name>

<description>  
VideoLAN project's media player
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
vlc
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
vlc
</uninstall_package_names>
</app>