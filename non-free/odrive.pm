<?xml version="1.0"?>
<app>

<category>
Network
</category>

<name>
Open Drive
</name>

<description>
Multi-service cloud storage sync client
</description>

<installable>
64
</installable>

<screenshot>none</screenshot>

<preinstall>
wget https://github.com/liberodark/ODrive/releases/download/0.2.1/odrive_0.2.1_amd64.deb
apt-get install ./odrive_0.2.1_amd64.deb
rm odrive_0.2.1_amd64.deb
</preinstall>

<install_package_names>

</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
odrive
</uninstall_package_names>
</app>
