<?xml version="1.0"?>
<app>

<category>
Remote Access
</category>

<name>
TeamViewer
</name>

<description>
Teamviewer remote access
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

( curl -RLJ https://download.teamviewer.com/download/linux/signature/TeamViewer2017.asc | apt-key add - ) 2>/dev/null 1>/dev/null
echo "deb http://linux.teamviewer.com/deb stable main" > /etc/apt/sources.list.d/teamviewer.list
apt-get update

</preinstall>

<install_package_names>
teamviewer
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
teamviewer
</uninstall_package_names>
</app>
