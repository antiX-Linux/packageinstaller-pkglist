<?xml version="1.0"?>
<app>

<category>
Audio
</category>

<name>
Spotify
</name>

<description>
Spotify from Spotify Testing Repo
</description>

<installable>
64
</installable>

<screenshot>none</screenshot>

<preinstall>
curl -sS https://download.spotify.com/debian/pubkey_0D811D58.gpg | sudo apt-key add -
echo "deb http://repository.spotify.com stable non-free">/etc/apt/sources.list.d/spotify.list
apt-get update
</preinstall>

<install_package_names>
spotify-client
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
spotify-client
</uninstall_package_names>
</app>
