<?xml version="1.0"?>
<app>

<category>
Media Center
</category>

<name>
Plex
</name>

<description>
Plex Mediaserver - includes web client. (requires reboot after install)
</description>

<installable>
all
</installable>

<screenshot>https://www.plex.tv/wp-content/uploads/2016/06/image-block-plex-media-server-macbook.jpg</screenshot>

<preinstall>
apt-get install apt-transport-https
curl https://downloads.plex.tv/plex-keys/PlexSign.key | apt-key add -
echo "deb https://downloads.plex.tv/repo/deb public main">/etc/apt/sources.list.d/mxpitemp.list
apt-get update
</preinstall>

<install_package_names>
plexmediaserver
</install_package_names>


<postinstall>
if [ -x /usr/sbin/start_pms ]; then
apt-get install plex-sysvinit-compat
fi
</postinstall>


<uninstall_package_names>
plexmediaserver
plex-sysvinit-compat
</uninstall_package_names>
</app>
