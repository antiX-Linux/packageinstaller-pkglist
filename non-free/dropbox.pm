<?xml version="1.0"?>
<app>

<category>
Network
</category>

<name>
Dropbox
</name>

<description>
Dropbox file sync utility
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
nautilus-dropbox
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
nautilus-dropbox
</uninstall_package_names>
</app>
