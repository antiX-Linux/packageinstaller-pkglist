<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
Portuguese
</name>

<description>  
Portuguese dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-pt-pt
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-pt-pt
</uninstall_package_names>

</app>
