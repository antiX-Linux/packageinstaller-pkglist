<?xml version="1.0"?>
<app>

<category>
Development
</category>

<name>
atom
</name>

<description>
atom text editor IDE
</description>

<installable>
64
</installable>

<screenshot>none</screenshot>

<preinstall>
wget -qO - https://packagecloud.io/AtomEditor/atom/gpgkey | apt-key add -
echo "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main">/etc/apt/sources.list.d/atom.list
apt-get update
</preinstall>

<install_package_names>
atom
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
atom
</uninstall_package_names>
</app>
