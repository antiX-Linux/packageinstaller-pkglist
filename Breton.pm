<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>  
Breton
</name>

<description>  
Breton dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
hunspell-br
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-br
</uninstall_package_names>

</app>
